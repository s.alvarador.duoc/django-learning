from django.urls import path
from django.conf.urls.static import static
from django.conf import settings

from . import views


app_name = 'blog'

urlpatterns = [
    path('', views.post_list, name='index'),
    path('detail/<int:post_id>', views.post_detail, name='detail'),
    path('create', views.post_create, name='create'),
    path('sesion', views.admin, name='admin'),
]

