from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from .models import Post
from django.utils import timezone
from django.urls import reverse
from django.contrib.auth import authenticate, login, logout

# Create your views here.
# https://docs.djangoproject.com/en/2.1/topics/db/queries/
# https://docs.djangoproject.com/es/2.1/topics/auth/default/


def admin(request):
    if request.method == 'GET':
        if 'logout' in request.GET:
            if request.user.is_authenticated:
                logout(request)
            return HttpResponseRedirect('admin')
        elif 'nouser' in request.GET:
            return render(request,'blog/admin.html', { 'mensaje' : 'Acceso denegado' })
        elif request.user.is_authenticated:
            return HttpResponseRedirect(reverse('blog:index'))

        return render(request, 'blog/admin.html', {})
    elif request.method == 'POST':
        username = request.POST['user']
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        if user is not None:
            # Para agregar variables en session debe escribir la siguiente línea:
            # request.session['nomre_variable'] = valor_variable
            login(request, user)
            return HttpResponseRedirect(reverse('blog:index'))

    return HttpResponseRedirect('admin?nouser')


def index(request):
    return HttpResponse("Hola mundo")


def post_list(request):
    posts = None
    if request.user.is_authenticated:
        posts = Post.objects.all().order_by('published_date')
    else:
        posts = Post.objects.filter(
            published_date__lte=timezone.now()).order_by('published_date')
    error = None
    if 'error' in request.session:
        error = request.session['error']
        del request.session['error']
        
    return render(request, 'blog/post_list.html', {'posts': posts, 'error': error})


def post_create(request):
    if not request.user.is_authenticated or request.user.has_perm('blog_add') == False:
        request.session['error'] = "Sin autorización para agregar nuevos post"
        return HttpResponseRedirect(reverse('blog:index'))

    if request.method == 'GET':
        return render(request, 'blog/post_create.html', {})
    elif request.method == 'POST':
        message = ""

        try:
            post = Post()
            post.author = request.user
            post.title = request.POST.get('title')
            post.text = request.POST.get('text')
            published_date = request.POST.get('published_date')
            
            #post.published_date = lambda published_date : None if published_date == "" else request.POST.get('published_date')
            post.save()
            file = request.FILES['file']
            ext = file.name.split(".")[-1]
            file.name = str(post.pk) + "." + ext
            post.file = file
            post.save()
            message = "Éxito al guardar la publicación"
            return HttpResponseRedirect(reverse('blog:detail', args=(post.pk,)))
        except Exception as e:
            message = "Error al guardar la publicación: " + str(e)
            return render(request, 'blog/post_create.html', {'message': message})
    else:
        response = HttpResponse('Método no permitido')
        response.status_code = 405
        return response


def post_detail(request, post_id):
    post = get_object_or_404(Post, pk=post_id)
    if request.method == 'POST':
        post.publish()

    try:
        return render(request, 'blog/post_detail.html', {'post': post})
    except Exception as e:
        response = HttpResponse("Error al guardar la publicación: " + str(e))
        response.status_code = 404
        return response
